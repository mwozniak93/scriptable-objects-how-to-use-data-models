﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//public 
public interface IShootableData
{
	int Ammo{get;set;}
	float FireRate{get;set;}
	float FirePower{get;set;}
	GameObject Bullet {get;set;}
}
[CreateAssetMenu(menuName="Shooting Data Compose")]
public class ShootingDataCompose :  ScriptableObject 
{

	[SerializeField]
	 public int ammo;
	[SerializeField]
	public float fireRate;
	[SerializeField] 
	public float firePower;
	[SerializeField]
	public GameObject bullet;
}
