﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
[CreateAssetMenu(menuName="Base Entity")]
public  class EntityBase : ScriptableObject {

	public string Name;
	public int Id;
	public List<ScriptableObject> ComposeableDataForComponents;

	public ScriptableObject GetDataForComponent<T>(){
		var dataComponent =ComposeableDataForComponents.Where (x => x is T).FirstOrDefault ();
		Debug.Log ("GetDataForComponent : " + dataComponent.GetType ().ToString ());
		return dataComponent;
	}

}
