﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName="Defense Data Compose")]
public class DefenseDataCompose : ScriptableObject {
	[SerializeField]
	float defense;
	public float Defense {
		get { return defense; }
		set{defense = value; }
	}
}
