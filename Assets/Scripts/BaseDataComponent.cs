﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDataInitiable
{
	void Init (EntityBase dataModel);
}
public class BaseDataComponent : MonoBehaviour {
	public EntityBase itemModel;
	// Use this for initialization
	void Start () {
		foreach (var item in GetComponents<IDataInitiable>()) {
			item.Init (itemModel);
		}
	}
	

}
