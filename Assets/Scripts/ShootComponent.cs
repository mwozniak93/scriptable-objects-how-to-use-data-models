﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IComponentShootable
{
	ShootingDataCompose Model{ get; set; }
	IEnumerator Shoot ();
}

public class ShootComponent : MonoBehaviour, IDataInitiable, IComponentShootable {

	ShootingDataCompose  model;

	public ShootingDataCompose Model {
		get {
			return model;
		}
		set {
			model = value;
		}
	}

	// Use this for initialization
	void Start () {
		
	}

	#region IItemInit implementation


	public void Init (EntityBase itemModel)
	{
		ShootingDataCompose componentData =  itemModel.GetDataForComponent<ShootingDataCompose> () as ShootingDataCompose;
		Debug.Log ("componentData ; " + componentData.ammo);
		model = componentData;
		Debug.Log ("model.Ammo : " + model.ammo);
	}


	#endregion

	public IEnumerator Shoot(){
		yield return new WaitForSeconds (model.fireRate);
		GameObject bulletGO =  Instantiate (model.bullet);
		bulletGO.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (100f, 0));
		StartCoroutine (Shoot ());
	}
	

}
